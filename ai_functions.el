﻿;; General functions
(defun ol/get-keyword-key-value (kwd)
  (let ((data (cadr kwd)))
    (list (plist-get data :key)
          (plist-get data :value))))

(defun ol/org-current-buffer-get-prop (prop)
  (nth 1
       (assoc prop
              (org-element-map (org-element-parse-buffer 'greater-element)
                  '(keyword)
                #'ol/get-keyword-key-value))))

(defun ol/org-file-get-prop (file prop)
  (with-temp-buffer
    (insert-file-contents file)
    (ol/org-current-buffer-get-prop prop))
  )


;; docs functions
(defun os-walk-docs (root)
  (let ((files '()) ;empty list to store results
        (current-list (directory-files root t)))
    ;;process current-list
    (while current-list
      (let ((fn (car current-list))) ; get next entry
        (cond 
         ;; regular files
         ((file-regular-p fn)
          (add-to-list 'files (file-relative-name fn ".")))
         ;; directories
         ((and
           (file-directory-p fn)
           ;; ignore . and ..
           (not (string-equal ".." (substring fn -2)))
           (not (string-equal "." (substring fn -1))))
          ;; we have to recurse into this directory
          (setq files (append files (os-walk-docs (concat fn "/")))))
         )
        ;; cut list down by an element
        (setq current-list (cdr current-list)))
      )
    (list `(:dir ,root  :files ,files))))

(defun prep-file-docs (files pre)
  (mapconcat
   (lambda (x) (when (string= (file-name-extension x) "org")(concat pre (format "[[file:%s][%s]]\n" x (ol/org-file-get-prop x "TITLE")))))
   files
   ""))

(defun get-eles-docs (prep files)
  (remq nil (mapcar (lambda (x) (when (funcall prep x) x)) files)))

(defun prep-ele-docs (ele pre)
  (let ((dir (plist-get ele :dir))
        (files (plist-get ele :files)))

    (if (or (listp (car files))
            (< 1 (length (cl-remove-if (lambda (x) (and (char-or-string-p x) (file-name-extension x)))files))))
        (concat
         ;; Skip docs parent folder and index
         (unless (string= "docs" (file-name-nondirectory (directory-file-name (file-name-directory dir))))
           (concat pre
                   (format "@@html:<div class=\"collapse_dov_nav\"><span class=\"fa fa-chevron-right\">%s</span><div class=\"doc_nav_child hidden\">@@\n"
                           (file-name-nondirectory (directory-file-name (file-name-directory dir))))))
         (unless (string= "docs" (file-name-nondirectory (directory-file-name (file-name-directory dir))))
           (prep-file-docs (get-eles-docs 'char-or-string-p files) (concat "  " pre)))
         (mapconcat (lambda (x) (prep-ele-docs x (concat "  " pre))) (get-eles-docs 'listp files) "")
         (unless (string= "docs" (file-name-nondirectory (directory-file-name (file-name-directory dir))))
           (concat pre "@@html:</div></div>@@\n")))
      (prep-file-docs (get-eles-docs 'char-or-string-p files) pre))))


;; Include Files
(defun prepend-to-file (text fname)
  "Prepend `TEXT' to file `FNAME'"
  (with-temp-buffer
    ;; first: insert `text' into temp buffer
    (insert text)
    ;; next: insert contents of `fname' into temp buffer
    (insert-file-contents fname)
    ;; finally: write whole buffer to `fname'
    (write-region (point-min) (point-max) fname nil nil)))

(defun ol/prepend-file-content-to-file (INSERT REGEX FILENAME)
  (let ((file-string (with-temp-buffer
                       (insert-file-contents FILENAME)
                       (buffer-string))))
    (if (string-match-p
         REGEX
         file-string)
        (unless (string-match-p (regexp-quote INSERT) file-string)
          (with-temp-buffer
            (insert-file-contents FILENAME)
            (replace-regexp REGEX INSERT)
            (write-region (point-min) (point-max) FILENAME nil nil)))
      (prepend-to-file (concat INSERT "\n") FILENAME))))

(defun ol/append-file-content-to-file (INSERT REGEX FILENAME)
  (let ((file-string (with-temp-buffer
                       (insert-file-contents FILENAME)
                       (buffer-string))))
    (if (string-match-p
         REGEX
         file-string)
        (unless (string-match-p (regexp-quote INSERT) file-string)
          (with-temp-buffer
            (insert-file-contents FILENAME)
            (replace-regexp REGEX INSERT)
            (write-region (point-min) (point-max) FILENAME nil nil)))
      (write-region (concat "\n\n\n\n" INSERT) nil FILENAME t nil))))


(defun ol/prep-basic-header (PLIST FILENAME PUB-DIR)
  (ol/prepend-file-content-to-file
   (format "#+INCLUDE: %sincludes/header.org" ol/ai-root-path)
   "#\\+INCLUDE: .*/includes/header\\.org"
   FILENAME))

(defun ol/append-basic-footer (PLIST FILENAME PUB-DIR)
  (ol/append-file-content-to-file
   (format "#+INCLUDE: %sincludes/footer.org" ol/ai-root-path)
   "#\\+INCLUDE: .*/includes/footer\\.org"
   FILENAME))


(defun ol/prep-docs-header (PLIST FILENAME PUB-DIR)
  (ol/prepend-file-content-to-file
   (format "#+INCLUDE: %sincludes/doc_header.org" ol/ai-root-path)
   "#\\+INCLUDE: .*/includes/doc_header\\.org"
   FILENAME))

(defun ol/append-docs-footer (PLIST FILENAME PUB-DIR)
  (ol/append-file-content-to-file
   (format "#+INCLUDE: %sincludes/doc_footer.org" ol/ai-root-path)
   "#\\+INCLUDE: .*/includes/doc_footer\\.org"
   FILENAME))


(defun ol/prep-papers-header (PLIST FILENAME PUB-DIR)
  (ol/prepend-file-content-to-file
   (format "#+INCLUDE: %sincludes/paper_header.org" ol/ai-root-path)
   "#\\+INCLUDE: .*/includes/paper_header\\.org"
   FILENAME))

(defun ol/append-papers-footer (PLIST FILENAME PUB-DIR)
  (ol/append-file-content-to-file
   (format "#+INCLUDE: %sincludes/paper_footer.org" ol/ai-root-path)
   "#\\+INCLUDE: .*/includes/paper_footer\\.org"
   FILENAME))

;; Papers functions
(defun os-walk-projects (root)
  (let ((files '()) ;empty list to store results
        (current-list (directory-files root t)))
    ;;process current-list
    (while current-list
      (let ((fn (car current-list))) ; get next entry
        (cond 
         ;; regular files
         ((file-regular-p fn)
          (add-to-list 'files fn))
         ;; directories
         ((and
           (file-directory-p fn)
           ;; ignore . and ..
           (not (string-equal ".." (substring fn -2)))
           (not (string-equal "." (substring fn -1))))
          ;; we have to recurse into this directory
          (setq files (append files (os-walk-projects fn))))
         )
        ;; cut list down by an element
        (setq current-list (cdr current-list)))
      )
    files))

(defun ol/get-paper-style (res)
  (cond ((string= res "Video")
         "fa fa-film")
        ((string= res "Article")
         "fa fa-newspaper-o")
        ((string= res "Book")
         "fa fa-book")
        ((string= res "Code")
         "fa fa-code")
        ((string= res "Paper")
         "fa fa-file-text-o")))

(defun ol/paper-nav (dir)
  (mapconcat 'identity (mapcar 
                        (lambda (x)
                          (format "[[file:%s][%s]]" (file-relative-name  x (file-name-directory (f-this-file)))
                                  (concat (format "@@html:<i class=\"%s\"></i>@@"
                                                  (ol/get-paper-style (ol/org-file-get-prop x "RES")))
                                          (ol/org-file-get-prop x "TITLE"))))
                        (cl-remove-if-not 
                         (lambda (x) (and (string= (file-name-extension x) "org")
                                          (not (string-match "index.org" x))
                                          (not (string-match "all_papers.org" x))))
                         (sort (os-walk-projects dir)
                               (lambda (x y) (org-time>
                                              (ol/org-file-get-prop x "DATE")
                                              (ol/org-file-get-prop y "DATE"))))))
             "\n"))
