---
title: Introduction - Artificial Intelligence
permalink: /docs/ai/intro/
img_folder: ai/intro
---

Artificial Intelligence (AI) is a big topic. To summarize it in a vague sentence:   

`Understanding the human intelligence and/or building it.`

There are more multiple definitions, that come from a different angle to describe this subject.
Stuart Russel and Peter Norvig created a table in there
book *Artificial Intelligence: A Modern Approach*. This table gives 8 definitions from 4 different
perspectives:

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">Thinking Humanly</th>
<th scope="col" class="org-left">Thinking Rationally</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">“The exciting new effort to make computers think . . . machines with minds, in the full and literal sense.” (Haugeland, 1985)</td>
<td class="org-left">“The study of mental faculties through the use of computational models.” (Charniak and McDermott, 1985)</td>
</tr>


<tr>
<td class="org-left">“[The automation of] activities that we associate with human thinking, activities such as decision-making, problem solving, learning . . .” (Bellman, 1978)</td>
<td class="org-left">“The study of the computations that make it possible to perceive, reason, and act.” (Winston, 1992)</td>
</tr>
</tbody>

<tbody>
<tr>
<td class="org-left"><b>Acting Humanly</b></td>
<td class="org-left"><b>Acting Rationally</b></td>
</tr>
</tbody>

<tbody>
<tr>
<td class="org-left">“The art of creating machines that perform functions that require intelligence when performed by people.” (Kurzweil,1990)</td>
<td class="org-left">“Computational Intelligence is the study of the design of intelligent agents.” (Poole et al., 1998)</td>
</tr>


<tr>
<td class="org-left">“The study of how to make computers do things at which, at the moment, people are better.” (Rich and Knight, 1991)</td>
<td class="org-left">“AI . . . is concerned with intelligent behavior in artifacts.” (Nilsson, 1998)</td>
</tr>
</tbody>
</table>

As seen in this table a big split in AI can be make: thinking and behavior. These two points can
be related but doesn't have to.

The field of AI is split into multiple subfields two subfields are *Machine Learning*, the learning
from example and *Deep Learning* as a subfield of *Machine Learning*.
These subjects raise in popularity in recent years. Especially the field of Deep Learning increased
with the achievement in computer vision.




