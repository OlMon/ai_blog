---
title: Introduction - Machine Learning
permalink: /docs/ml/intro/
img_folder: ml/intro
---
Machine Learning (ML) is a subset of Artificial Intelligence, that focuses of algorithms that
improve by example.

The algorithms improves by providing data called "trainings data" and the algorithm learns to
predict the appropriate output without explanation how to do so.

The approaches can be traditionally divided into three categories:

-   Supervised learning
-   Unsupervised learning
-   Reinforcement learning

\\
Machine Learning is used in a variety of fields: Computer vision, natural language processing and
general game playing to name a few.

In recent years a subfield of Machine Learning
achieved big success and evolved to its own bis subject: *Deep Learning*.

![categorization]({{page.img}}/{{page.img_folder}}/categories.jpg)
*Subject classification*
