﻿#+INCLUDE: ./header.org

#+begin_src emacs-lisp :wrap "mininav" :exports results
(concat "#+HTML:<a href=\"/ai_blog/papers/all_papers.html\">All Papers</a>\n" "*Last Papers*\n" (ol/paper-nav (concat ol/ai-root-path "papers/")))
#+end_src

#+HTML:<div id="content-text">

#+HTML: <h1>
{{{title}}}
#+HTML:</h1>



#+TOC: headlines 2
