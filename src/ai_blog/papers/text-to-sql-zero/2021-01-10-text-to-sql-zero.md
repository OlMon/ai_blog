---
title: Zero-shot Text-to-SQL Learning with Auxiliary Task
date: 10.01.2021
source: https://arxiv.org/abs/1908.11052
released: 29.09.2019
author: Shuaichen Chang, Pengfei Liu, Yun Tang, Jing Huang, Xiaodong He, Bowen Zhou
topic: DL
tags: [sql, nlp, lstm]
format: paper
img_folder : text-sql-zero
show: True
---

# Table of Contents

1.  [Summary](#org63687ef)


<a id="org63687ef"></a>

# Summary

With the recent success of neural seq2seq models for text to SQL translation, some questions
are raised in regards how models generalize with unseen data.

The authors diagnose the bottleneck and propose a new testbed. Additionally the authors designed

> a simple but effective auxil-iary task, which serves as a supportive modelas well as
> a regularization term to the gener-ation  task  to  increase  the  models  generalization.

![model]({{page.img}}/{{page.img_folder}}/model.png)
*Figure 3: Illustration of our model. The upper figure
is the text-to-SQL generation model which consists of threeparts: encoder (lower left),
AGG/SEL decoder (upper left) and where decoder (upper right).
Lower right isWHEREdecoder cell. The bottom figure is our auxiliary mapping model with the
ground-truth label of an example. Questionword is mapped to a column only when it is tagged
as part of a condition value (BvorIv).*

